<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Vision/light specification on SpamAssassin training in PPSMBE 10.0](#visionlight-specification-on-spamassassin-training-in-ppsmbe-100)
    - [About the document](#about-the-document)
- [Feature overview](#feature-overview)
    - [The implementation concept](#the-implementation-concept)
    - [Prototyping results](#prototyping-results)
- [Implementation](#implementation)
    - [Distinguishing ham, spam and ignored messages](#distinguishing-ham-spam-and-ignored-messages)
        - [Folder lists](#folder-lists)
        - [Path traversal](#path-traversal)
    - [Training script](#training-script)
        - [Script execution and logging](#script-execution-and-logging)
        - [Executing sa-learn](#executing-sa-learn)
        - [Performance issues](#performance-issues)
        - [Error handling](#error-handling)
    - [Platform-dependent implementation](#platform-dependent-implementation)
        - [Unix systems](#unix-systems)
        - [Windows systems](#windows-systems)
    - [Other considerations](#other-considerations)
    - [Additional features](#additional-features)
    - [Possible features](#possible-features)
    - [Open issues](#open-issues)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Vision/light specification on SpamAssassin training in PPSMBE 10.0

## About the document

This document provides general information about the Apache SpamAssassin Training feature in Plesk Panel Small Business Edition (PPSMBE or *the application* further).

# Feature overview

[Apache SpamAssassin][sa-home]  is an email filter that detects spam with a set of rules called **tests**.
Effectiveness of each test contributes to the overall performance.

SpamAssassin includes a [Bayesian classifier][sa-bayesian] test which implements [Bayesian filtering][wiki-bayesian] – a statistical algorithm capable of machine learning.
It works out-of-the-box, but to achieve high accuracy it should learn on example emails with [`sa-learn` utility][sa-learn].

PPSBME 9.x already has an implementation of SpamAssassin training that lacks usability.
See the [9.x documentation][training-in-9.x] for further details.

> // todo: "Usability" is somewhat ambiguous.
What were the problems of the previous implementation?
How will the current implementation solve them?

In PPSMBE 10.0 we are going to implement the new *SpamAssassin Training* feature.
It should comply to the following criteria:

1. SpamAssassin should learn on both spam and ham to prevent biased evaluation.
1. The training should be regular and use all available learning material.
1. The implementation should be natural and transparent to the user.

[sa-home]: https://spamassassin.apache.org/
[wiki-bayesian]: https://en.wikipedia.org/wiki/Naive_Bayes_spam_filtering
[sa-bayesian]: https://wiki.apache.org/spamassassin/BayesInSpamAssassin
[sa-learn]: https://spamassassin.apache.org/full/3.1.x/doc/sa-learn.html
[training-in-9.x]: https://example.com

## The implementation concept

This implementation seems the most reasonable:

* The application considers that particular IMAP folders contain only spam or ham messages.
* It runs `sa-learn` regularly on such messages.
* The user only has to move emails to the particular folders to help train the SpamAssassin.

## Prototyping results

The following environments have been tested with a feature prototype:

* PPSMBE Windows + SmarterMail + Horde
* PPSMBE Unix + atMail.

The whole feature idea looks applicable and works well in general.

# Implementation

## Distinguishing ham, spam and ignored messages

### Folder lists

Email categorization is based on the assumption that both SpamAssassin and the user:

 1. Can reliably distinguish spam and ham.
 2. Always move those to certain IMAP folders.

The following diagram shows simplified user scenarios and email states:

![Email message states diagram](http://www.plantuml.com/plantuml/png/1S713O0m2030LNG0uUysM8cI0cqArRYzTwjuqNdo7BhScJrsn4jIwO3I35rAXQVfvrKSktBKBJaI1xre5Ca3sKWK-ZJz0G00)

<!--
http://www.plantuml.com/plantuml/uml/5Smn3e0W3030tLVe0wIx4r-fs6X3gOGMZRzNfzikccFpNcIOM9FqZNgJyN2ljW3ix89hI6S1vPJ3TSgZcHMgaENPoHmQtb3Gd1fGGPPG_osIRfyl
-->

The application should distinguish emails to be used in training as spam or ham in the following way:

* A hardcoded `spam-folders-list` should include:

  * Base names of all folders that supported email clients use for spam.
   There is no uniform name and most clients use their own ones.
  * "SpamLearn", the name of the default spam folder. The application should provide it for those clients that do not create a spam folder themselves.

  First version of the list should include "SpamLearn" (default) and ".Spam", which is used by *atMail*.
* Another hardcoded `ignore-folders-list` should include base names of all folders like "Trash", "Sent" and "Drafts" which can contain both ham and spam.
* The application does not need a list for ham folders and should instead consider ham to be any mail that:
  * is in folders outside of `spam-folders-list` and `ignore-folders-list`;
  * has been read by the user.

> // todo: "hardcoding" is an ambiguous term.
Which exactly should the location and format of both lists be?

The `spam-folders-list` and `ignore-folders-list` can only contain [base names][basename], i.e. can not contain paths.
For example, `Spam` is allowed and `INBOX/MyMail/Spam` is not.

Both `spam-folders-list` and `ignore-folders-list` can further extend to support more email clients.
During the development the team should check all email clients supported by PPSMBE and extend both lists accordingly.

> // todo: Should the application allow the administrator to manually extend both lists?

The application should create a `SpamLearn` IMAP folder immediately on the mail account creation.
This should be done whether the SpamAssassin is turned on or not.

> //todo: Should not the same be done for existing accounts if PPSMBE is updated to a newer version with SA Training?

**Note**: the [`mchk` command][mchk] will not create a `SpamLearn` folder if it is absent.

The application should allow the user to create and remove any folders from both lists. For example:

* If the user deletes all folders from `spam-folders-list`, SpamAssassin will not be trained on spam for this user.
* If the user creates a new folder matching the `spam-folders-list`, SpamAssassin will be trained on messages from this folder.

[basename]: https://en.wikipedia.org/wiki/Basename
[mchk]: https://support.plesk.com/hc/en-us/articles/213360709

### Path traversal

As said above, both folder lists only contain base names.
The application should recursively traverse paths from the mail root, considering only the base names.
In the process it should populate spam and ham lists which it will use for training.

The following diagram explains the path traversal algorithm:

![folder traversal diagram][traversal]

[traversal]: http://www.plantuml.com/plantuml/png/1S7H4O0W203GLMg0vBzjqBZYHFG0xTg-ztRM8lEYQR8z4SDFW9j3CAVI6oYNcbQNJoih326lHv076BxGq8CCmd2HEKeQiyaF

<!--
http://www.plantuml.com/plantuml/uml/5Son4OCm30NGFbEG0R3wLAmYZ8xuB0kVz0t7zWdLwzuIOCTe-deNopes6QxJ5-Zn8TeBbDUKZqPMSatdeRVLOjHLeiwG03bVr3WWJd0-nOCrzMTSnBQ_Fm00
-->

## Training script

### Script execution and logging

The application should use a script `sa-train.psa` to execute `sa-learn`.
The script should traverse folders from mail root to find ham and spam messages on each execution.

The DailyMaintenanceScript should execute `sa-train.psa` once in a period of time, probably once a day.
`sa-train.psa` should log events if run separately, but not if it is executed by DailyMaintenanceScript.

### Executing sa-learn

The `sa-train.psa` should process each spam message with

    sa-learn --no-sync --spam <filename>

and each ham message with

    sa-learn --no-sync --ham <filename>

At the end of the training the following should be run:

    sa-learn --sync

See the [sa-learn documentation][sa-learn] for details on sa-train parameters.

**Note**: `--sync` and `--no-sync` are explained further in Performance issues

**Note**: If execution parameters should be changed, edit the code blocks above and explain changes in detail in appropriate sections of the document.

### Performance issues

* SpamAssassin doesn't learn on already processed messages.
Thus for better performance the script should:
  * Remember the last execution time.
  * Only use messages received later than this time.

* By default, `sa-learn` synchronizes the database and journal after each call. With several consecutive calls it is more effective to use `--no-sync` to skip that and then run `sa-learn --sync` once.

* The script should only process messages of size less than a certain threshold.
It can use the value of the "Maximum e-mail message size" setting of PPSMBE, located at <kbd>Settings</kbd> > <kbd>Mail Server Settings</kbd>.

  > // todo: The script is unable to browse the application GUI.
  The document should describe the way to get this value programmatically or provide a link.

  For implementation details see the [documentation on PPSMBE settings][settings-doc].

[settings-doc]: example.com

### Error handling

* If the script encounters an error during training it should send a mail notification to admin.

 > Email notifications do not scale well.
 In a large-scale application it may be better to log the error and/or use a monitoring system.

* The script should not attempt to resume training after an error.
It is acceptable to leave some messages unprocessed.

## Platform-dependent implementation

### Unix systems

On Unix systems the application can use file system operations. which will be much faster than IMAP.

* To determine unprocessed messages the script can save the execution time by `touch`ing a special file on startup and using [`find -newer`][man-find]:

        touch <timestamp-file>
        find <path> -newer <timestamp-file> <other parameters>

> // todo: If the script calls `touch` before `find`, won't it find messages, received since milliseconds ago (i.e. nothing)?

* Read and unread messages could also be distinguished via file system. E.g. the [Courier-IMAP][courier-imap] stores metadata in files.
* Courier-IMAP [uses file system subfolders to store messages][courier-imap-maildirs]. Thus the script should probably process `Maildir/cur` and `SpamLearn/cur` instead of just `Maildir/*` and `SpamLearn/cur`. This should be investigated further.

[man-find]: http://linuxcommand.org/man_pages/find1.html
[courier-imap]: http://www.courier-mta.org/imap/
[courier-imap-maildirs]: http://www.courier-mta.org/imap/maildirs.html


### Windows systems

On Windows systems the application can use IMAP to create folders and get messages.
IMAP [provides server-side search][imap-search-example], which can be used to determine messages for training.

Developers should first measure the performance of IMAP usage.
If it appears too low, they should instead implement direct file system usage, which is also possible but requires mailserver-specific code.
For example, [MailEnable] and [SmarterMail] both use simple text files to store messages.

[imap-search-example]: http://www.example-code.com/csharp/imap-search-critera.asp
[MailEnable]: https://www.mailenable.com/
[SmarterMail]: https://www.smartertools.com/smartermail/business-email-server

## Other considerations


1. The [`spammng` command][spammng] should probably not be used for learning:
    * There seems to be no training option in Windows version.
    * In a test it worked with `MailDir` folder but not with `.Spam` folder.
   Thus it is better to call `sa-learn` directly.

   > //todo: could not find documentation on `spammng`, it is probably deprecated.

[spammng]: http://example.com

## Additional features

1. The application should rotate spam.
 It should delete already processed messages which are older than a specified period of time (month by default).
 This period should be set in a configuration file (not in GUI).


## Possible features

1. `sa-train.psa` can count the execution time and stop if execution time is too long.
This feature may become a major one if the training performance is too low or if the script processes a huge amount of mail.

1. (*Probably off topic*) The application can provide predefined server-side mail filters to move spam messages into folders from `spam-folders-list`.
The user/administrator should be able to turn this off.
This can be implemented with several solutions:
    1. Using [sieve filters][sieve-filters].
        * MailEnable does not support this according to its support forum.
        Indeed, this should be checked again, same for SmarterMail
        * Postfix does not seem to support this by default, but there are several implementations of the sieve language, which can be called from procmail.
    1. Using procmail.
        * Native for Unix.
        * Absent on Windows, but it seems possible to implement a similar mechanism or use built-in filter mechanisms of supported mail servers, if they are available.
    1. Using IMAP to search and move messages:
        * The application may move messages along with spam training.
        * IMAP is cross-platform.
        * The user might not be happy with the application moving messages between IMAP folders.

    Of the three solutions the second seems most applicable: using procmail on Unix and developing an analogue for Windows.

[sieve-filters]: http://www.faqs.org/rfcs/rfc3028.html

## Open issues

1. There are options for where to run training:
    * server-side, or
    * on each mailname.

  The proposed solution is to try and keep the current state.
  We should address this issue again after implementing the learning feature.

  > I'm not quite sure that I understood this right.
    * //todo: What exactly is the current state or "as it is now"?
    * //todo: Is it the Postfix mailname?
     A link would look great here.

1. There is an issue with false learning, which can happen in the following way:
    1. SA wrongly marks an incoming message as spam.
    1. `sa-train.psa` executes and passes the message to `sa-learn --spam`.
    1. The user marks the message as ham and moves it to some ham folder.
    1. `sa-train.psa` will not retrain SA on next execution because the message was received before the last execution.

  This issue is not critical if `sa-train.psa` uses file system to get message dates.
  There seems to be no good solution at this moment.
  We should probably ignore this issue now and address it later.
