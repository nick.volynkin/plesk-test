# Secure passwords and how to remember them

Some websites on the Internet allow you to publish information and talk to friends, like Facebook or Twitter. Others store personal and sensitive data, like email and bank websites. To make sure you are the right person they ask for:

 * a non-secret [account name][username],
 * and a secret [password].

Like this:

![](img/username_and_password.png)

 A [hacker] can easily guiess a simple password like this:

    qwerty

A complex password is hard to guess, so it is [more secure][password-security]:

    |q2}dHUeO\"^cT5`

You should better use complex passwords to protect your accounts.
Remembering them is hard, but your browser can safely do it for you.

When you enter your password, the browser will offer to
 remember it *for this website only*, like this:

![](img/save_pwd_safari.png)

Next time you open the same page the browser will fill it for you:

![](img/remember_pwd_safari.png)

You can tell your browser to remember other things like credit card number, contact information and more.

# Safety tips

1. Only do this on your own computer, and never on computers in public places.
1. If somebody (like your children) is using your computer *with your account*, they can also use remembered passwords.
Make a new account for them (instructions for [Windows] and [macOS][macos]).

# See the detailed instructions for your browser:

[![](img/firefox.png)][firefox]
[![](img/chrome.png)][chrome]
[![](img/safari.png)][safari]
[![](img/ie.png)][ie]
[![](img/edge.png)][edge]

[username]: https://en.wikipedia.org/wiki/User_(computing)#User_account
[password]: https://en.wikipedia.org/wiki/Password
[password-security]: https://en.wikipedia.org/wiki/Password#Choosing_a_secure_and_memorable_password
[hacker]: https://en.wikipedia.org/wiki/Hacker
[Windows]: https://support.microsoft.com/en-us/help/13951/windows-create-user-account
[macos]: https://support.apple.com/kb/PH18891?locale=en_GB

[chrome]: https://support.google.com/chrome/answer/95606?co=GENIE.Platform%3DDesktop&hl=en-GB
[firefox]: https://support.mozilla.org/en-US/kb/password-manager-remember-delete-change-and-import
[safari]: https://support.apple.com/kb/PH21470?locale=en_US&viewlocale=en_US
[ie]: https://support.microsoft.com/en-us/help/17499/windows-internet-explorer-11-remember-passwords-fill-out-web-forms
[edge]: https://support.microsoft.com/en-us/instantanswers/6da36bea-792a-439a-b2c9-21703ca39fbc/remember-passwords-in-microsoft-edge